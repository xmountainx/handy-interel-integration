package com.handy.client.interel.services;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class InterelClientTests {

    private final String JWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBrZXkiOiJFMDAwMDAwMFQyMDAwMDAxIiwiZGV2aWNlaWQiOiJoYW5keS1jbGllbnQiLCJyb29tbnVtYmVyIjozMTIsImV4cCI6NjM2NDk3MTEwNjAxNzMyMjI3LCJwYWlyaW5nSWQiOiJGRTAwMDAwMFUyMDAwMDAxIn0.5PXFOsMMa8sRRtV97kSby9jvRANMCNuxY9Pqx5izXIQ";
    private final String OK_RESPONSE = "\"OK\"";

	@Autowired
	private InterelClient client;


	@Test
	public void getApiStatus() throws Exception {
		boolean status = client.getApiStatus();
		Assert.assertTrue(status);
	}


	@Test
	public void requestJwt() throws Exception {
		String jwt = client.requestJwt("312");

		System.out.println(jwt);
		Assert.assertNotNull(jwt);
	}

    @Ignore
	@Test
	public void requestPairCode() throws Exception {
		String result = client.requestPairCode(JWT);
		Assert.assertNotNull(result);
	}

    @Ignore
	@Test
	public void pairDevice() throws Exception {
		String result = client.pairDevice(JWT, 5709);
		Assert.assertNotNull(result);
	}


	@Test
	public void getRoomState() throws Exception {
        Map<String, Object> result = client.getRoomState(JWT);
        Assert.assertNotNull(result);
    }


	@Test
	public void setBlindState() throws Exception {
        String result = client.setBlindState(JWT, "B01", "ON");
        Assert.assertEquals(OK_RESPONSE, result);
	}


	@Test
	public void setCurtainState() throws Exception {
        String result = client.setCurtainState(JWT, "Curtain1", "Open");
        Assert.assertEquals(OK_RESPONSE, result);
	}


	@Test
	public void setDnd() throws Exception {
        String result = client.setDnd(JWT, "Activate");
        Assert.assertEquals(OK_RESPONSE, result);
	}


	@Test
	public void setFanMode() throws Exception {
        String result = client.setFanMode(JWT, "0", "Low");
        Assert.assertEquals(OK_RESPONSE, result);
	}

	@Test
	public void setLight() throws Exception {
//        client.setLight(JWT, "BedsideLeft", "OFF");
//        client.setLight(JWT, "BedsideRight", "ON");
//        client.setLight(JWT, "FloorLamp", "ON");
//        client.setLight(JWT, "DayBedDownLight", "ON");
//        client.setLight(JWT, "BathroomCeiling", "ON");

        String result = client.setLight(JWT, "Master", "OFF");

        Assert.assertEquals(OK_RESPONSE, result);
	}

	@Test
	public void setMur() throws Exception {
        String result = client.setMur(JWT, "Activate");
        Assert.assertEquals(OK_RESPONSE, result);
	}


    @Test
    public void changeSetPoint() throws Exception {
        String result = client.changeSetPoint(JWT, "0", 20);
        Assert.assertEquals(OK_RESPONSE, result);
    }

	@Test
	public void subscribeWebHook() throws Exception {
        String result = client.subscribeWebHook(JWT, "312");
        Assert.assertTrue(result != null && result.contains("\"Id\": "));
    }

    @Ignore
    @Test
	public void deleteWebHook() throws Exception {
        String result = client.deleteWebHook(JWT, "312");
        Assert.assertEquals(OK_RESPONSE, result);
	}

}
