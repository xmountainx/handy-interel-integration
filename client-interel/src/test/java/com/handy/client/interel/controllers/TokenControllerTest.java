package com.handy.client.interel.controllers;

import com.handy.client.interel.services.TokenService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TokenControllerTest {

    @Autowired
    private TokenController controller;

    @Autowired
    private TokenService service;

    @Before
    public void setUp() throws Exception {
        service.setToken("301", "testing-token-301");
        service.setToken("302", "testing-token-302");
    }

    @Test
    public void listTokens() throws Exception {
        Map<String, String> result = controller.listTokens();
        Assert.assertEquals(service.getCache().size(), result.size());
    }

    @Test
    public void getToken() throws Exception {
        String token = controller.getToken("301");
        Assert.assertEquals("testing-token-301", token);
    }

    @Test
    public void setToken() throws Exception {
        Map<String, String> body = new HashMap<>();
        body.put("token", "testing-token-303");

        controller.setToken("303", body);
        Assert.assertTrue(service.getCache().containsKey("303"));
    }

    @Test
    public void removeToken() throws Exception {
        controller.removeToken("303");
        Assert.assertTrue(!service.getCache().containsKey("303"));
    }


}