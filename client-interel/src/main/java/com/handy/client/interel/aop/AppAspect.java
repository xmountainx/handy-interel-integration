package com.handy.client.interel.aop;


import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Arrays;
import java.util.Map;

@Slf4j
@Aspect
@Component
public class AppAspect {

//    @Around("@annotation(com.handy.client.interel.aop.RestOperation)")

    @Around("target(com.handy.client.interel.services.InterelClient) && execution(public * *(..))")
    public Object handleInterelClientMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        try {
            log.info("Intercept the method " + joinPoint.getSignature().getName() + ", " + Arrays.asList(joinPoint.getArgs()));

            Object retVal = joinPoint.proceed();

            log.info("Result: " + retVal);

            if (retVal instanceof Map) {
                Map<String, Object> response = (Map<String, Object>) retVal;

                if (response.containsKey("Error")) {
                    Map<String, String> error = (Map<String, String>) response.get("Error");
                    String code = error.get("errorCode");
                    String message = error.get("errorMessage");

                    throw new IllegalStateException("Interel error, code : " + error + ", message: " + message);
                }
            }

            return retVal;
        } catch (HttpClientErrorException e) {
            throw new IllegalStateException(e.getResponseBodyAsString(), e);
        }
    }

}
