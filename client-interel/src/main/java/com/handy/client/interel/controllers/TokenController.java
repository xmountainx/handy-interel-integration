package com.handy.client.interel.controllers;

import com.handy.client.interel.services.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/tokens")
public class TokenController {

    @Autowired
    private TokenService tokenService;

    @GetMapping
    public Map<String, String> listTokens() {
        return tokenService.getCache();
    }

    @GetMapping("/{roomNumber}")
    public String getToken(@PathVariable("roomNumber") String roomNumber) {
        return tokenService.getToken(roomNumber);
    }

    @PutMapping("/{roomNumber}")
    public void setToken(@PathVariable("roomNumber") String roomNumber, @RequestBody Map<String, String> body) {
        String token = body.get("token");
        tokenService.setToken(roomNumber, token);
    }

    @DeleteMapping("/{roomNumber}")
    public void removeToken(@PathVariable("roomNumber") String roomNumber) {
        tokenService.removeToken(roomNumber);
    }

}
