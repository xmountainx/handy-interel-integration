package com.handy.client.interel.services;

import com.handy.client.interel.aop.RestOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class InterelClient {

    @Value("${app.interel.url}")
    private String url;

    @Value("${app.interel.appKey}")
    private String appKey;

    @Value("${app.interel.webHookUrl}")
    private String webHookUrl;

    @Value("${app.interel.auth.deviceId}")
    private String deviceId;

    @Value("${app.interel.auth.lastName}")
    private String lastName;

    @Value("${app.interel.secret}")
    private String secret;

    @Autowired
    private CryptoService crypto;

    @Autowired
    private RestTemplate template;

    // health test
    public boolean getApiStatus() {
        ResponseEntity<Boolean> entity = template.getForEntity(url + "/roomcontrol/getApiStatus", Boolean.class);
        return entity.getBody();
    }

    // pairing
    public String requestJwt(String roomNumber) throws GeneralSecurityException {
        String payload = crypto.encode(roomNumber, deviceId, lastName);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url + "/roomcontrol/requestjwt")
                .queryParam("appkey", appKey)
                .queryParam("encryptedPayload", payload);

        ResponseEntity<String> response = template.postForEntity(
            builder.build().encode().toUri(),
            null,
            String.class
        );

        return response.getBody();
    }

    public String requestPairCode(String jwt) {
        HttpEntity<Object> request = createRequest(jwt);
        ResponseEntity<String> response = template.exchange(url + "/roomcontrol/requestPairingCode", HttpMethod.GET, request, String.class);

        return response.getBody();
    }

    public String pairDevice(String jwt, int pairingCode) {
        HttpEntity<Object> request = createRequest(jwt);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url + "/roomcontrol/pairDevice")
                .queryParam("pairingCode", pairingCode);

        ResponseEntity<String> response = template.postForEntity(
                builder.build().encode().toUri(),
                request,
                String.class
        );

        return response.getBody();
    }

    // business operations
    @RestOperation
    public Map<String, Object> getRoomState(String jwt) {
        HttpEntity<Object> request = createRequest(jwt);
        ResponseEntity<Map> response = template.exchange(
                url + "/roomcontrol/GetRoomState",
                HttpMethod.GET,
                request,
                Map.class
        );

        return response.getBody();
    }

    public String setBlindState(String jwt, String blindId, String state) {
        HttpEntity<Object> request = createRequest(jwt);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url + "/roomcontrol/setBlindState")
                .queryParam("blindId", blindId)
                .queryParam("state", state);

        ResponseEntity<String> response = template.exchange(
                builder.build().encode().toUri(),
                HttpMethod.PUT,
                request,
                String.class
        );

        return response.getBody();
    }

    public String setCurtainState(String jwt, String curtainId, String state) {
        HttpEntity<Object> request = createRequest(jwt);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url + "/roomcontrol/setCurtainState")
                .queryParam("curtainId", curtainId)
                .queryParam("state", state);

        ResponseEntity<String> response = template.exchange(
                builder.build().encode().toUri(),
                HttpMethod.PUT,
                request,
                String.class
        );

        return response.getBody();
    }

    public String setDnd(String jwt, String state) {
        HttpEntity<Object> request = createRequest(jwt);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url + "/roomcontrol/setDnd")
                .queryParam("state", state);

        ResponseEntity<String> response = template.exchange(
                builder.build().encode().toUri(),
                HttpMethod.PUT,
                request,
                String.class
        );

        return response.getBody();
    }

    public String setFanMode(String jwt, String thermostatId, String mode) {
        Map<String, Object> params = new HashMap<>();

        params.put("ThermostatId", thermostatId);
        params.put("FanMode", mode);

        HttpEntity<Map<String, Object>> request = createRequest(jwt, params);

        ResponseEntity<String> response = template.exchange(
                url + "/roomcontrol/setFanMode",
                HttpMethod.PUT,
                request,
                String.class
        );

        return response.getBody();
    }

    public String setLight(String jwt, String light, String state) {
        HttpEntity<Object> request = createRequest(jwt);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url + "/roomcontrol/setLight")
                .queryParam("light", light)
                .queryParam("state", state);

        ResponseEntity<String> response = template.exchange(
                builder.build().encode().toUri(),
                HttpMethod.PUT,
                request,
                String.class
        );

        return response.getBody();
    }

    public String setMur(String jwt, String state) {
        HttpEntity<Object> request = createRequest(jwt);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url + "/roomcontrol/SetMur")
                .queryParam("state", state);

        ResponseEntity<String> response = template.exchange(
                builder.build().encode().toUri(),
                HttpMethod.PUT,
                request,
                String.class
        );

        return response.getBody();
    }

    public String changeSetPoint(String jwt, String thermoId, int setPoint) {
        Map<String, Object> params = new HashMap<>();

        params.put("ThermostatId", thermoId);
        params.put("SetPoint", setPoint);

        HttpEntity<Object> request = createRequest(jwt, params);

        ResponseEntity<String> response = template.exchange(
                url + "/roomcontrol/changeSetpoint",
                HttpMethod.PUT,
                request,
                String.class
        );

        return response.getBody();
    }

    public String callScene(String jwt, String sceneName) {
        HttpEntity<Object> request = createRequest(jwt);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url + "/roomcontrol/sendScene")
                .queryParam("sceneName", sceneName);

        ResponseEntity<String> response = template.exchange(
                builder.build().encode().toUri(),
                HttpMethod.POST,
                request,
                String.class
        );

        return response.getBody();
    }

    // register webhook listener
    public String subscribeWebHook(String jwt, String roomNumber) {
        Map<String, Object> params = new HashMap<>();

        params.put("WebHookUri", webHookUrl);
        params.put("Secret", secret);
        params.put("Filter", new String[] { roomNumber });

        HttpEntity<Map<String, Object>> request = createRequest(jwt, params);
        ResponseEntity<String> response = template.postForEntity(url + "/WebHookSubscription/Subscribe", request, String.class);

        return response.getBody();
    }

    public String deleteWebHook(String jwt, String id) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url + "/WebHookSubscription/Delete")
                .queryParam("id", id);

        HttpEntity<Object> request = createRequest(jwt);

        ResponseEntity<String> response = template.exchange(
                builder.build().encode().toUri(),
                HttpMethod.DELETE,
                request,
                String.class
        );

        return response.getBody();
    }

    private <T> HttpEntity<T> createRequest(String jwt) {
        return createRequest(jwt, null);
    }

    private <T> HttpEntity<T> createRequest(String jwt, T body) {
        HttpHeaders headers = createHeaders(jwt);
        return new HttpEntity<>(body, headers);
    }

    private HttpHeaders createHeaders(String jwt) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        if (!StringUtils.isEmpty(jwt)) {
            headers.set("Authorization", "Bearer " + jwt);
        }

        return headers;
    }

}
