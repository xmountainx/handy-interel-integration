package com.handy.client.interel.services;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;

@Service
public class CryptoService {

    private static final String ALGORITHM = "RSA";

    @Value("${app.interel.publicKey}")
    private String publicKey;

    private String modulusKey;

    private String exponentKey;

    @PostConstruct
    public void init() {
        modulusKey = substrBetween(publicKey, "<Modulus>", "</Modulus>");
        exponentKey =  substrBetween(publicKey, "<Exponent>", "</Exponent>");
    }

    private String substrBetween(String str, String begin, String end) {
        int beginIdx = str.indexOf(begin);
        int endIdx = str.indexOf(end, beginIdx);

        if (beginIdx >= 0 && endIdx >= 0) {
            return str.substring(beginIdx + begin.length(), endIdx);
        }

        return str;
    }

    public String encode(String roomNumber, String deviceId, String lastName) throws GeneralSecurityException {
        String content = "{" +
                "  \"roomNumber\": \"${roomNumber}\", " +
                "  \"deviceId\": \"${deviceId}\", " +
                "  \"lastName\": \"${lastName}\"" +
                "}";

        content = content.replace("${roomNumber}", roomNumber)
                .replace("${deviceId}", deviceId)
                .replace("${lastName}", lastName);

        byte[] modBytes = Base64.decodeBase64(modulusKey);
        byte[] expBytes = Base64.decodeBase64(exponentKey);

        BigInteger modulus = new BigInteger(1, modBytes);
        BigInteger exponent = new BigInteger(1, expBytes);

        RSAPublicKeySpec pubSpec = new RSAPublicKeySpec(modulus, exponent);
        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM );
        PublicKey publicKey = keyFactory.generatePublic(pubSpec);

        Cipher cipher = Cipher.getInstance(ALGORITHM );
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);

        byte[] encrypted = cipher.doFinal(content.getBytes());
        String result = Hex.encodeHexString(encrypted);

        return result;
    }

}