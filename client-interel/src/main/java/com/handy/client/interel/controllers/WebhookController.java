package com.handy.client.interel.controllers;

import com.handy.client.connector.HandyGatewayConnector;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RestController
public class WebhookController {

    @Autowired
    private HandyGatewayConnector connector;

    @RequestMapping("/interel/webhook")
    public String handle(@RequestParam(value = "echo", required = false) String echo, @RequestBody(required = false) Map<String, String> params) throws Exception {
        if (echo != null && !echo.isEmpty()) {
            return echo;
        }

        if (params != null && !params.isEmpty()) {
            String notifyCode = params.get("NotifyCode");
            String eventDate = params.get("EventDate");
            String identifier = params.get("Identifier");
            String status = params.get("Status");

            JSONObject obj = new JSONObject(params);

            log.info("webhook got, notifyCode: " + notifyCode + ", identifier: " + identifier + ", eventDate:  " + eventDate + " status: " + status);

            switch (notifyCode) {

                case "DnD":
                    connector.send("onDndChange", obj);
                    break ;

                case "MuR":
                    connector.send("onMurChange", obj);
                    break ;

                case "SetPoint":
                    connector.send("onFanPointChange", obj);
                    break ;

                case "FanMode":
                    connector.send("onFanModeChange", obj);
                    break ;

                case "Curtain":
                    connector.send("onCurtainChange", obj);
                    break ;

                case "Blind":
                    connector.send("onBlindChange", obj);
                    break ;

                case "Light":
                    connector.send("onLightChange", obj);
                    break ;

                case "Presence":
                    connector.send("onPresenceChange", obj);
                    break ;

                default:
                    log.info("unknown webhook code: " + notifyCode);
            }
        }

        return "";
    }

}
