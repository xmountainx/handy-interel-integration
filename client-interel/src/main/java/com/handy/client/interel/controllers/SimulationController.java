package com.handy.client.interel.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/interelrestapi/api")
public class SimulationController {
    
    private final String SUCCESS = "\"OK\"";

    private final Map<String, Object> ERROR = mapOf("Error",
        mapOf("errorCode", "AuthenticationFailed", "errorMessage", "Server failed to authenticate the request, Token expired")
    );

    @GetMapping("/roomcontrol/getApiStatus")
    public boolean getApiStatus() {
        return true;
    }

    @PostMapping("/roomcontrol/requestjwt")
    public String requestJwt(@RequestParam String appkey, @RequestParam String encryptedPayload) {
        String roomNumber = encryptedPayload;
        String deviceId = encryptedPayload;
        String lastName = encryptedPayload;

        return "jwt.token.xxxx";
    }

    @GetMapping("/roomcontrol/requestPairingCode")
    public String requestPairCode(@RequestHeader("Authorization") String authorization) {
        log.info("SimulationController.requestPairCode ");
        log.info("authorization = [" + authorization + "]");

        return SUCCESS;
    }

    @PostMapping("/roomcontrol/pairDevice")
    public String pairDevice(@RequestHeader("Authorization") String authorization, @RequestParam int pairingCode) {
        log.info("SimulationController.pairDevice");
        log.info("authorization = [" + authorization + "], pairingCode = [" + pairingCode + "]");

        return SUCCESS;
    }

    // business operations
    @GetMapping("/roomcontrol/GetRoomState")
    public Map<String, Object> getRoomState(@RequestHeader("Authorization") String authorization) {
        log.info("SimulationController.getRoomStatus");
        log.info("authorization = [" + authorization + "]");

        Map<String, Object> status = new HashMap<>();
        Map<String, Object> thermo = new HashMap<>();

        status.put("Dnd", true);
        status.put("Mur", false);

        thermo.put("Identifier", "XXXXXXXX");
        thermo.put("ActualTemp", 27.54);
        thermo.put("FanMode", "High");
        thermo.put("Setpoint", 25.0);

        status.put("DevThermo", thermo);

        return status;
    }

    @PutMapping("/roomcontrol/setBlindState")
    public String setBlindState(@RequestHeader("Authorization") String authorization, @RequestParam String blindId, @RequestParam String state) {
        log.info("SimulationController.setBlindState");
        log.info("authorization = [" + authorization + "], blindId = [" + blindId + "], state = [" + state + "]");

        return SUCCESS;
    }

    @PutMapping("/roomcontrol/setCurtainState")
    public String setCurtainState(@RequestHeader("Authorization") String authorization, @RequestParam String curtainId, @RequestParam String state) {
        log.info("SimulationController.setCurtainState");
        log.info("authorization = [" + authorization + "], curtainId = [" + curtainId + "], state = [" + state + "]");

        return SUCCESS;
    }

    @PutMapping("/roomcontrol/setDnd")
    public String setDnd(@RequestHeader("Authorization") String authorization, @RequestParam String state) {
        log.info("SimulationController.setDnd");
        log.info("authorization = [" + authorization + "], state = [" + state + "]");

        return SUCCESS;
    }

    @PutMapping("/roomcontrol/setFanMode")
    public String setFanMode(@RequestHeader("Authorization") String authorization, @RequestBody Map<String, Object> params) {
        log.info("SimulationController.setFanMode");
        log.info("authorization = [" + authorization + "], params = [" + params + "]");

        return SUCCESS;
    }

    @PutMapping("/roomcontrol/setLight")
    public String setLight(@RequestHeader("Authorization") String authorization, @RequestParam String light, @RequestParam String state) {
        log.info("SimulationController.setLight");
        log.info("authorization = [" + authorization + "], light = [" + light + "], state = [" + state + "]");

        return SUCCESS;
    }

    @PutMapping("/roomcontrol/SetMur")
    public String setMur(@RequestHeader("Authorization") String authorization, @RequestParam String state) {
        log.info("SimulationController.setMur");
        log.info("authorization = [" + authorization + "], state = [" + state + "]");

        return SUCCESS;
    }

    @PutMapping("/roomcontrol/changeSetpoint")
    public String changeSetpoint(@RequestHeader("Authorization") String authorization, @RequestBody Map<String, Object> params) {
        log.info("SimulationController.setMur");
        log.info("authorization = [" + authorization + "], params = [" + params + "]");

        return SUCCESS;
    }

    @PostMapping("/WebHookSubscription/Subscribe")
    public String subscribe(@RequestHeader("Authorization") String authorization, @RequestBody Map<String, Object> params) {
        log.info("SimulationController.Subscribe");
        log.info("authorization = [" + authorization + "], params = [" + params + "]");

        return SUCCESS;
    }

    @DeleteMapping("/WebHookSubscription/Delete")
    public String deleteWebHook(@RequestHeader("Authorization") String authorization, @RequestParam String id) {
        log.info("SimulationController.deleteWebHook");
        log.info("authorization = [" + authorization + "], id = [" + id + "]");

        return SUCCESS;
    }

    private Map<String, Object> mapOf(Object... values) {
        Map<String, Object> map = new HashMap<>();

        for (int i = 0; i < values.length; i += 2) {
            String k = (String )values[i];
            Object v = values[i + 1];

            map.put(k, v);
        }

        return map;
    }

}
