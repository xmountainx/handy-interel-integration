package com.handy.client.interel.services;

import com.handy.client.connector.HandyGatewayConnector;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Service
public class HandyHandler {

    private static final JSONObject SUCCESS = new JSONObject("{ \"success\" : true }");

    @Autowired
    private HandyGatewayConnector connector;

    @Autowired
    private InterelClient client;

    @Autowired
    private TokenService ts;

    @PostConstruct
    void init() throws Exception {
        HandyHandler handler = this;

        // RoomState
        connector.onMessage("getRoomState", (payload) -> {
            String roomNumber = payload.getString("roomNumber");
            String token = ts.getToken(roomNumber);

            return new JSONObject(client.getRoomState(token));
        });

        connector.onMessage("setRoomState", (payload) -> {
            String roomNumber = payload.getString("roomNumber");
            String token = ts.getToken(roomNumber);


            if (payload.has("fanPoint")) {
                int fanPoint = payload.getInt("fanPoint");
                client.changeSetPoint(token, "0", fanPoint);
            }

            if (payload.has("fanMode")) {
                String fanMode = payload.getString("fanMode");
                client.setFanMode(token, "0", fanMode);
            }

            if (payload.has("dnd")) {
                boolean dnd = payload.getBoolean("dnd");
                client.setDnd(token, dnd ? "Activate" : "Deactivate");
            }

            if (payload.has("mur")) {
                boolean mur = payload.getBoolean("mur");
                client.setDnd(token, mur ? "Activate" : "Deactivate");
            }

            return SUCCESS;
        });

        // Blind and Curtain
        connector.onMessage("setBlind", (payload) -> {
            String roomNumber = payload.getString("roomNumber");
            String status = payload.getString("status");

            String token = ts.getToken(roomNumber);
            client.setBlindState(token, "", status);

            return SUCCESS;
        });

        connector.onMessage("setCurtain", (payload) -> {
            String roomNumber = payload.getString("roomNumber");
            String status = payload.getString("status");

            String token = ts.getToken(roomNumber);
            client.setCurtainState(token, "", status);

            return SUCCESS;
        });

        // Lights
        connector.onMessage("setLight", (payload) -> {
            String roomNumber = payload.getString("roomNumber");
            String lightType = payload.getString("lightType");
            String status = payload.getString("status");

            String token = ts.getToken(roomNumber);
            client.setLight(token, lightType, status);

            return SUCCESS;

            // BedsideLeft
            // BedsideRight
            // FloorLamp
            // DayBedDownLight
            // BathroomCeiling
            // BathroomMirror
        });

    }

}
