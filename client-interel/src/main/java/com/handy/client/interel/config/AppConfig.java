package com.handy.client.interel.config;

import com.handy.client.connector.HandyGatewayConnector;
import com.handy.client.connector.MqttGatewayConnector;
import com.handy.client.connector.SocketGatewayConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableAspectJAutoProxy
public class AppConfig {

    @Value("${app.handy.gateway.url}")
    private String url;

    @Value("${app.handy.gateway.token}")
    private String token;

    @Value("${app.handy.gateway.namespace}")
    private String namespace;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Autowired
    org.springframework.core.env.Environment env;

    @Bean
    public HandyGatewayConnector handyConnector() throws Exception {
        HandyGatewayConnector connector = new SocketGatewayConnector();

        connector.connect(url, token);
        connector.setNamespace("interel");

        return connector;
    }

}