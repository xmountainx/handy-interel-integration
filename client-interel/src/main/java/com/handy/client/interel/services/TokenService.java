package com.handy.client.interel.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

@Service
public class TokenService {

    @Autowired
    private InterelClient client;

    private Map<String, String> cache = new HashMap<>();

    public String getToken(String roomNumber) {
        try {
            String token = cache.get(roomNumber);

            if (!cache.containsKey(roomNumber)) {
                token = client.requestJwt(roomNumber);
                cache.put(roomNumber, token);
            }

            return token;
        } catch (GeneralSecurityException e) {
            throw new IllegalStateException(e);
        }
    }

    public void setToken(String roomNumber, String token) {
        cache.put(roomNumber, token);
    }

    public void removeToken(String roomNumber) {
        cache.remove(roomNumber);
    }

    public Map<String, String> getCache() {
        return cache;
    }

}
