package com.handy.client.interel.controllers;


import com.handy.client.interel.services.InterelClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/api")
public class CertController {

    @Autowired
    private InterelClient client;

    @Autowired
    private RestTemplate template;

    @Value("${app.interel.url}")
    private String url;

    @Value("${app.interel.secret}")
    private String secret;

    @GetMapping("requestJwt")
    public String requestJwt(@RequestParam String appKey, @RequestParam String publicKey, @RequestParam String payload) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url + "/roomcontrol/requestjwt")
                .queryParam("appkey", appKey)
                .queryParam("encryptedPayload", payload);

        ResponseEntity<String> response = template.postForEntity(
                builder.build().encode().toUri(),
                null,
                String.class
        );

        return response.getBody();
    }

    @GetMapping("requestPairCode")
    public String requestPairCode(@RequestParam String jwt) {
        return client.requestPairCode(jwt);
    }

    @GetMapping("pairDevice")
    public String pairDevice(@RequestParam String jwt, @RequestParam int code) {
        return client.pairDevice(jwt, code);
    }

    @GetMapping("getRoomState")
    public Map<String, Object> getRoomState(@RequestParam String jwt) {
        return client.getRoomState(jwt);
    }

    @GetMapping("thermoSetpoint")
    public String thermoSetpoint(@RequestParam String jwt, @RequestParam String identifier, @RequestParam int point) {
        return client.changeSetPoint(jwt, identifier, point);
    }

    @GetMapping("thermoFanMode")
    public String thermoFanMode(@RequestParam String jwt, @RequestParam String identifier, @RequestParam String mode) {
        return client.setFanMode(jwt, identifier, mode);
    }

    @GetMapping("setDnd")
    public String setDnd(@RequestParam String jwt, @RequestParam String state) {
        return client.setDnd(jwt, state);
    }

    @GetMapping("setMur")
    public String setMur(@RequestParam String jwt, @RequestParam String state) {
        return client.setMur(jwt, state);
    }

    @GetMapping("setCurtains")
    public String setCurtains(@RequestParam String jwt, @RequestParam String curtainId, @RequestParam String state) {
        return client.setCurtainState(jwt, curtainId, state);
    }

    @GetMapping("setBlinds")
    public String setBlinds(@RequestParam String jwt, @RequestParam String blindId, @RequestParam String state) {
        return client.setBlindState(jwt, blindId, state);
    }

    @GetMapping("setLights")
    public String setLights(@RequestParam String jwt, @RequestParam String type, @RequestParam String state) {
        return client.setLight(jwt, type, state);
    }

    @GetMapping("callSense")
    public String callSense(@RequestParam String jwt, @RequestParam String sceneName) {
        return client.callScene(jwt, sceneName);
    }

    @GetMapping("subscribeWebhook")
    public String subscribeWebhook(@RequestParam String jwt, @RequestParam String webhookUri, @RequestParam String roomNumber) {
        Map<String, Object> params = new HashMap<>();
        String result = null;

        params.put("WebHookUri", webhookUri);
        params.put("Secret", secret);
        params.put("Filter", new String[] { roomNumber });

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);

            if (!StringUtils.isEmpty(jwt)) {
                headers.set("Authorization", "Bearer " + jwt);
            }

            HttpEntity<Map<String, Object>> entity = new HttpEntity<>(params, headers);
            ResponseEntity<String> response = template.postForEntity(url + "/WebHookSubscription/Subscribe", entity, String.class);

            result = response.getBody();
        } catch (HttpClientErrorException ex) {
            ex.printStackTrace();
            result = ex.getResponseBodyAsString();
        }

        return result;
    }



}
