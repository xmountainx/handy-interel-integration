package com.handy.client.connector;


import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;

import java.util.Date;
import java.util.Optional;

public class Main {

    //

    public static void main(String[] args) throws Exception {
        HandyGatewayConnector connector = new SocketGatewayConnector();
        connector.connect("http://localhost:3000", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpbnRlZ3JhdGlvbl9pZCI6IjEifQ.E9ZBxWixl9XiZqNxcRxc-J-bIIDMHt3ful3ZKH0pyb1JBqftkqN7-w8lZO8z8RJ2t9GSlfexLvrh-7gucfbxeZxa8YW4QAbalnSSw_uwBOssygNRMUxc0QsFa7DI1IMecfuK_ySNwuTC6mFgqnrj0oOp2of-ljqtjo0AYy1paweBC7LhGAnE6wy0WBoQrYY85-k1bc4yN55LNqp5TVt7Qdl9COgcqCO42RObAPI2gBR0gRO5DcL9jrZGqRVCwVDgTbEE1uiBqniz4xnEx7SQXwdKO_FNIesyb788X_vkjhapF76V6sB1P3yx38IbUnR-EvoflD9C02EYkrX0ZG1LJp5uIN870BGpDq784r7yeAKHXgwXkXanUlqdLzleEZApqteFWxmz9bScoR70xxI1UWJIORmegc_8GUHBWoUTgCRdeM5AiuOHum2up9Z45rkYPpgH7jJJ4aqb46qmfz6ouss-lzRkd0drNJHRKs4-hDYwwihSOzCoyFIj-UQdu2gRy78YW6opkxArAsFt8CmKiiGdtofdSKrLTtqXIRzDidVtEibgDwkT-Ae-QHXsYD7ENZb1qqXnzNJQlIm6eY-hhtgDdnLlH6j2xBDTGiBmTxYo1XzkQJK1pBMctd2X0FwKwohy6Py6rt_LtGE3rUKmZdKU5xnluUjpYV1EsumvqOY");


//        HandyGatewayConnector connector = new MqttGatewayConnector();
//        connector.setNamespace("interel");
//        connector.connect("tcp://192.168.99.100:1883", "username1");

        JSONObject obj = new JSONObject();
        obj.put("success", true);
        obj.put("send_time", new Date());

        connector.send("testing", obj, (response) -> {
            System.out.println("response: " + response.getClass());
        });

        connector.onMessage("room/light", (message) -> {
            System.out.println("message: " +  message);
            return null;
        });

        Thread.sleep(2000);

        connector.close();
    }

}