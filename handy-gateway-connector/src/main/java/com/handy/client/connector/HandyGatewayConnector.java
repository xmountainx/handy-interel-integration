package com.handy.client.connector;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONObject;

public interface HandyGatewayConnector {

    void connect(String url, String token) throws Exception;

    void close() throws Exception;

    void send(String topic, JSONObject payload, ResponseListener responseListener) throws Exception;

    void onMessage(String topic, OnMessageListener listener) throws Exception;

    void onEvent(String event, OnEventListener listener);

    default void setNamespace(String namespace) {

    }

    default void send(String topic, JSONObject payload) throws Exception {
        this.send(topic, payload, null);
    }

    interface OnMessageListener {

        JSONObject onMessage(JSONObject payload);

    }

    interface OnEventListener {

        void onEvent(Object... args);

    }

    interface ResponseListener {

        void onResponse(JSONObject response);

    }

}