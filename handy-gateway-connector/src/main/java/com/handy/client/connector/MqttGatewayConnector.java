package com.handy.client.connector;


import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;


public class MqttGatewayConnector implements HandyGatewayConnector {

    private static final Logger logger = LoggerFactory.getLogger(MqttGatewayConnector.class);

    private long messageId;
    private String namespace = "";
    private MqttClient client;

    private Map<String, OnEventListener> eventListeners;

    public MqttGatewayConnector() {
        eventListeners = new HashMap<>();
    }

    @Override
    public void connect(String url, String token) throws MqttException {
        client = new MqttClient(url, token, new MemoryPersistence());

        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setCleanSession(true);
        connOpts.setAutomaticReconnect(true);

        // Connecting to broker
        logger.info("Connecting to broker: " + url);
        client.connect(connOpts);

        logger.info("Connected");
        raiseEvent("connect");

        client.setCallback(new MqttCallbackHandler());
    }

    @Override
    public void close() throws Exception {
        if (client != null) {
            if (client.isConnected()) {
                client.disconnect();
            }

            client.close();
        }
    }

    @Override
    public void send(String topic, JSONObject payload, ResponseListener responseListener) throws Exception {
        String messageId = nextMessageId();
        MqttMessage message = new MqttMessage();

        payload = payload.put("topic", topic);

        message.setPayload(payload.toString().getBytes());
        message.setQos(1);
        message.setRetained(true);

        if (responseListener != null) {
            String responseTopic = namespace + "response/" + messageId;

            client.subscribe(responseTopic, (topic1, newMessage) -> {
                byte[] newPayload = newMessage.getPayload();
                JSONObject obj = new JSONObject(new String(newPayload));

                responseListener.onResponse(obj);
                client.unsubscribe(responseTopic);
            });
        }

        client.publish(namespace + topic, message);
    }

    @Override
    public void onMessage(String topic, OnMessageListener listener) throws Exception {
        client.subscribe(namespace + topic, new IMqttMessageListener() {

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                byte[] payload = message.getPayload();
                JSONObject obj = new JSONObject(new String(payload));

                JSONObject result = listener.onMessage(obj);
                if (result != null) {
                    String responseTopic = "response/" + message.getId();
                    MqttGatewayConnector.this.send(responseTopic, result);
                }
            }

        });
    }

    @Override
    public void onEvent(String event, OnEventListener listener) {
        eventListeners.put(event, listener);
    }

    @Override
    public void setNamespace(String namespace) {
        this.namespace = namespace + "/";
    }

    private synchronized String nextMessageId() {
        return String.valueOf(++messageId);
    }

    private void raiseEvent(String event, Object... args) {
        if (eventListeners.containsKey(event)) {
            OnEventListener listener = eventListeners.get(event);
            listener.onEvent(args);
        }
    }

    class MqttCallbackHandler implements MqttCallback {

        @Override
        public void connectionLost(Throwable cause) {
            logger.info("Connection lost");
            raiseEvent("disconnect", cause);
        }

        @Override
        public void messageArrived(String topic, MqttMessage message) throws Exception {
            logger.info("message arrived, topic: " + topic + " messageId: " + message.getId());
            raiseEvent("message", message);
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken token) {
            logger.info("Delivery complete, token: " + token.getMessageId());
        }

    }

}
