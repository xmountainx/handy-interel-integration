package com.handy.client.connector;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.Transport;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class SocketGatewayConnector implements HandyGatewayConnector, Closeable {

    private static final Logger logger = Logger.getLogger(SocketGatewayConnector.class.getName());

    private Socket socket;
    private Map<String, OnEventListener> eventListeners;
    private Map<String, OnMessageListener> messageListeners;

    public SocketGatewayConnector() {
        eventListeners = new HashMap<>();
        messageListeners = new HashMap<>();
    }

    @Override
    public void connect(String url, String token) throws Exception {
        IO.Options opts = new IO.Options();
        opts.path = "/socket";
        opts.forceNew = true;
        opts.reconnection = true;
        opts.transports = new String[] { "websocket" };

        socket = IO.socket(url, opts);

        logger.info("connect to socket IO server, url: " + url);

        // setup auth
        socket.io().on(Manager.EVENT_TRANSPORT, args -> {
            Transport transport = (Transport) args[0];

            transport.on(Transport.EVENT_REQUEST_HEADERS, args1 -> {
                Map<String, List<String>> headers = (Map<String, List<String>>) args1[0];
                headers.put("Authorization", Arrays.asList("Bearer " + token));
            });
        });

        // basic event
        socket.on(Socket.EVENT_CONNECT, args -> {
            logger.info("Socket server connected");
            raiseEvent("connect", args);
        }).on(Socket.EVENT_DISCONNECT, args -> {
            logger.info("Socket server disconnected");
            raiseEvent("disconnect", args);
        }).on(Socket.EVENT_ERROR, args -> {
            logger.info("Socket server error");
            raiseEvent("error", args);
        });

        // messages dispatcher
        socket.on("pms_command", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                try {
                    /*
                     request sample

                     obj -> {
                        id   : "xxxx"
                        data : {
                            "topic"   : "xxxx",
                            "payload" : { ... data for method ... }
                        }
                     }
                     */

                    JSONObject obj = (JSONObject) args[0];
                    String requestId = obj.getString("id");
                    JSONObject rawData = obj.getJSONObject("data");
                    String topic = rawData.getString("topic");
                    JSONObject payload = rawData.getJSONObject("payload");

                    if (!messageListeners.containsKey(topic)) {
                        logger.info("message receviced but not listener registered");
                        return ;
                    }

                    // process message
                    OnMessageListener listener = messageListeners.get(topic);
                    JSONObject result = listener.onMessage(payload);

                    // feedback ack if needed, emit
                    socket.emit(requestId, result);

                    /*
                    Object lastArgs = args[args.length - 1];
                    if (lastArgs instanceof Ack) {
                        Ack ack = (Ack) lastArgs;
                        ack.call(result);
                    }
                    */

                } catch (JSONException e) {
                    logger.warning("message pms_command first args is expected as JSON");
                }
            }

        });


        socket = socket.connect();
    }

    @Override
    public void close() {
        if (socket != null) {
            socket.close();
        }
    }

    @Override
    public void send(String topic, JSONObject payload, ResponseListener responseListener) throws Exception {
        Ack ack = null;

        if (responseListener != null) {
            ack = new Ack() {

                @Override
                public void call(Object... args) {
                    JSONObject obj = (JSONObject) args[0];
                    responseListener.onResponse(obj);
                }

            };
        }

        socket.emit("pms_data", payload, ack);
    }

    @Override
    public void onMessage(String topic, OnMessageListener listener) {
        messageListeners.put(topic, listener);
    }

    @Override
    public void onEvent(String event, OnEventListener listener) {
        eventListeners.put(event, listener);
    }

    private void raiseEvent(String event, Object... args) {
        if (eventListeners.containsKey(event)) {
            OnEventListener listener = eventListeners.get(event);
            listener.onEvent(args);
        }
    }

}
