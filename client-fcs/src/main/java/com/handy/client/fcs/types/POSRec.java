package com.handy.client.fcs.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class POSRec {

    @JacksonXmlProperty(localName = "EntryType", isAttribute = true)
    private String entryType;

    @JacksonXmlProperty(localName = "Code", isAttribute = true)
    private String code;

    @JacksonXmlProperty(localName = "Price", isAttribute = true)
    private String price;

    @JacksonXmlProperty(localName = "Qty", isAttribute = true)
    private String qty;

    @JacksonXmlProperty(localName = "Desc", isAttribute = true)
    private String desc;

    @JacksonXmlProperty(localName = "Disc", isAttribute = true)
    private String disc;

    @JacksonXmlProperty(localName = "SubTot", isAttribute = true)
    private String subTot;

    @JacksonXmlProperty(localName = "Ref", isAttribute = true)
    private String ref;

}
