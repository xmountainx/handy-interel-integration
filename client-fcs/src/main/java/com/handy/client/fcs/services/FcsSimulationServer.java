package com.handy.client.fcs.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

@Slf4j
@Service
public class FcsSimulationServer {

    private static final int STX = 0x02;
    private static final int ETX = 0x03;
    private static final int BUFFER_SIZE = 0xffff;

    private ServerSocket serverSocket;
    private Socket socket;

    private BufferedInputStream in;
    private BufferedOutputStream out;
    private ReceiveHandler handler;

    public void listen(String host, int port) throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);
        log.info("listening to port : " + port);

        Socket socket = serverSocket.accept();

        log.info("connection made. " + socket.getRemoteSocketAddress().toString());

        in = new BufferedInputStream(socket.getInputStream());
        out = new BufferedOutputStream(socket.getOutputStream());

        // setup listening thread
        handler = new ReceiveHandler(in)  {

            @Override
            public void onCommandReceived(byte[] buffer) {
                FcsSimulationServer.this.onCommandReceived(new String(buffer));
            }

        };

        handler.run();
    }

    public void sendCommand(String command) throws IOException {
        out.write(Constant.STX);
        out.write(command.getBytes());
        out.write(Constant.ETX);

        out.flush();
    }

    public void onCommandReceived(String command) {
        log.info("server get : " + command);
    }

    public void close() {
        handler.stop();

        closeQuietly(in);
        closeQuietly(out);
        closeQuietly(socket);
        closeQuietly(serverSocket);
    }

    private void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                // ignore
            }
        }
    }

}
