package com.handy.client.fcs;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.handy.client.fcs.types.Chg;
import com.handy.client.fcs.models.MTCharge;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootApplication
@EnableAutoConfiguration(exclude = {
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class
})
public class MainApplication {

    public static void main(String[] args) throws Exception {

        JacksonXmlModule module = new JacksonXmlModule();
        XmlMapper xmlMapper = new XmlMapper(module);

        xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
        xmlMapper.setDateFormat(new SimpleDateFormat("YYYY/MM/dd HH:mm:ss"));



//        RoomStatus roomStatus = new RoomStatus();
//
//        roomStatus.setEx("713061");
//        roomStatus.setRm("13061");
//        roomStatus.setPId("MO5");
//        roomStatus.setRsCode("VC");
//        roomStatus.setOldRsCode("VD");
//        roomStatus.setTransDaTi(new Date());
//        roomStatus.setSysDaTi(new Date());
//        roomStatus.setGuid("E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6");

        MTCharge obj = new MTCharge();

        Chg chg = new Chg();
        chg.setTot("12.00");

        obj.setEx("4567");
        obj.setPid("1");
        obj.setSysDaTi(new Date());
        obj.setTransDaTi(new Date());
        obj.setChg(chg);
        obj.setGuid("E1A35954-41CC-40f7-BDF1- A7B2CE1EE5B6");

        String str = xmlMapper.writeValueAsString(obj);

        System.out.println(str);
        System.out.println("end of test");





//        ConfigurableApplicationContext ctx = SpringApplication.run(MainApplication.class, args);
//
//        FcsSimulationServer server = ctx.getBean(FcsSimulationServer.class);
//        FcsClient connector = ctx.getBean(FcsClient.class);
//
//        try {
//            new Thread(() -> {
//                try {
//                    server.listen("localhost", 5001);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }).start();
//
//            Thread.sleep(2000);
//
//            connector.connect("localhost", 5001);
//            connector.sendCommand("testing");
//
//            server.sendCommand("XXXXX fucking");
//
//
//        } catch (IOException | InterruptedException e) {
//            e.printStackTrace();
//        } finally {
//
//            try {
//                Thread.sleep(2000);
//
//                server.close();
//                connector.close();
//
//                Thread.sleep(2000);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }
    }

}

