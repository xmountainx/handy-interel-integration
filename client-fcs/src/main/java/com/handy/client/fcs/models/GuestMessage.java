package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class GuestMessage extends BaseEntity {

    @JacksonXmlProperty(localName = "MesgType")
    private String mesgType;

    @JacksonXmlProperty(localName = "GFolNo")
    private String gFolNo;

    @JacksonXmlProperty(localName = "MesgId")
    private String mesgId;

    @JacksonXmlProperty(localName = "MesgContent")
    private String mesgContent;

    @JacksonXmlProperty(localName = "MesgDaTi")
    private Date mesgDaTi;

    @JacksonXmlProperty(localName = "Flag")
    private String flag;

}
