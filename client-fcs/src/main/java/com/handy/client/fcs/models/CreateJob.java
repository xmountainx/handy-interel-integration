package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class CreateJob extends BaseEntity {

    @JacksonXmlProperty(localName = "JobAction")
    private String jobAction;

    @JacksonXmlProperty(localName = "UserName")
    private String userName;

    @JacksonXmlProperty(localName = "Password")
    private String password;

    @JacksonXmlProperty(localName = "LangCode")
    private String langCode;

    @JacksonXmlProperty(localName = "ServiceType")
    private String serviceType;

    @JacksonXmlProperty(localName = "ItemCode")
    private String itemCode;

    @JacksonXmlProperty(localName = "LocationCode")
    private String locationCode;

    @JacksonXmlProperty(localName = "ScheduledDaTi")
    private Date scheduledDaTi;

    @JacksonXmlProperty(localName = "StartDaTi")
    private Date startDaTi;

    @JacksonXmlProperty(localName = "EndDaTi")
    private Date endDaTi;

    @JacksonXmlProperty(localName = "Rem")
    private String rem;

    @JacksonXmlProperty(localName = "Qty")
    private String qty;

    @JacksonXmlProperty(localName = "RunnerUserName")
    private String runnerUserName;

    @JacksonXmlProperty(localName = "RunnerId")
    private String runnerId;

    @JacksonXmlProperty(localName = "CreatedByUserName")
    private String createdByUserName;

    @JacksonXmlProperty(localName = "ReportedByName")
    private String reportedByName;

}
