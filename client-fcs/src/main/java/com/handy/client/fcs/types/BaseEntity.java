package com.handy.client.fcs.types;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

import java.util.Date;

@Data
public class BaseEntity {

    @JacksonXmlProperty(localName = "Ex")
    private String ex;

    @JacksonXmlProperty(localName = "Rm")
    private String rm;

    @JacksonXmlProperty(localName = "PId")
    private String pid;

    @JacksonXmlProperty(localName = "SysDaTi")
    private Date sysDaTi;

    @JacksonXmlProperty(localName = "GUID")
    private String guid;

}
