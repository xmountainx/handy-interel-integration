package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class UpdateJob extends BaseEntity {

    @JacksonXmlProperty(localName = "JobAction")
    private String jobAction;

    @JacksonXmlProperty(localName = "UserName")
    private String userName;

    @JacksonXmlProperty(localName = "Password")
    private String password;

    @JacksonXmlProperty(localName = "JobId")
    private String jobId;

    @JacksonXmlProperty(localName = "JobNo")
    private String jobNo;

    @JacksonXmlProperty(localName = "LangCode")
    private String langCode;

    @JacksonXmlProperty(localName = "RunnerId")
    private String runnerId;

    @JacksonXmlProperty(localName = "RunnerUserName")
    private String runnerUserName;

    @JacksonXmlProperty(localName = "ReportedByName")
    private String reportedByName;

    @JacksonXmlProperty(localName = "Rem")
    private String rem;

    @JacksonXmlProperty(localName = "NotifyDevice")
    private String notifyDevice;

    @JacksonXmlProperty(localName = "ItemCode")
    private String itemCode;

}
