package com.handy.client.fcs.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import com.handy.client.fcs.types.JobResponseRec;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class CreateJobResponse extends BaseEntity {

    @JacksonXmlProperty(localName = "AnsSts")
    private String ansSts;

    @JacksonXmlProperty(localName = "Flag")
    private String flag;

    @JacksonXmlProperty(localName = "ErrorMsg")
    private String errorMsg;

    @JacksonXmlProperty(localName = "EscalationId")
    private String escalationId;

    @JacksonXmlProperty(localName = "EscalationName")
    private String escalationName;

    @JacksonXmlElementWrapper(localName = "JobResponseRecSet")
    private List<JobResponseRec> jobResponseRecSet;

}
