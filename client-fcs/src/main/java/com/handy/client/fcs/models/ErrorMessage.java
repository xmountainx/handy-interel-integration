package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class ErrorMessage {

    @JacksonXmlProperty(localName = "PId")
    private String pid;

    @JacksonXmlProperty(localName = "AnsSts")
    private String ansSts;

    @JacksonXmlProperty(localName = "Desc")
    private String desc;

    @JacksonXmlProperty(localName = "SysDaTi")
    private String sysDaTi;

    @JacksonXmlProperty(localName = "GUID")
    private String guid;

}
