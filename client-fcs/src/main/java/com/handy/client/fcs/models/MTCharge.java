package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import com.handy.client.fcs.types.Chg;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class MTCharge extends BaseEntity {

    @JacksonXmlProperty(localName = "Chg")
    private Chg chg;

    @JacksonXmlProperty(localName = "TransDaTi")
    private Date transDaTi;

    @JacksonXmlProperty(localName = "MaidId")
    private String maidId;

    @JacksonXmlProperty(localName = "MaidPIN")
    private String maidPIN;

}
