package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class ModifyJob extends BaseEntity {

    @JacksonXmlProperty(localName = "JobNo")
    private String jobNo;

    @JacksonXmlProperty(localName = "JobDetail")
    private String jobDetail;

    @JacksonXmlProperty(localName = "EscLevel")
    private String escLevel;

    @JacksonXmlProperty(localName = "ItemCode")
    private String itemCode;

    @JacksonXmlProperty(localName = "ItemName")
    private String itemName;

    @JacksonXmlProperty(localName = "AssignedTo")
    private String assignedTo;

    @JacksonXmlProperty(localName = "Deadline")
    private String deadline;

    @JacksonXmlProperty(localName = "GuestName")
    private String guestName;

    @JacksonXmlProperty(localName = "LocationName")
    private String locationName;

    @JacksonXmlProperty(localName = "JobStatus")
    private String jobStatus;

    @JacksonXmlProperty(localName = "RunnerUserName")
    private String runnerUserName;

    @JacksonXmlProperty(localName = "ACKStatus")
    private String ackStatus;

    @JacksonXmlProperty(localName = "ACKDaTi")
    private Date ackDaTi;

    @JacksonXmlProperty(localName = "Sender")
    private String sender;

    @JacksonXmlProperty(localName = "ErrorMsg")
    private String errorMsg;

}
