package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ser.Serializers;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class RoomStatus extends BaseEntity {

    @JacksonXmlProperty(localName = "RSCode")
    private String rsCode;

    @JacksonXmlProperty(localName = "OldRSCode")
    private String oldRsCode;

    @JacksonXmlProperty(localName = "TransDaTi")
    private Date transDaTi;

    @JacksonXmlProperty(localName = "UserId")
    private String userId;

}
