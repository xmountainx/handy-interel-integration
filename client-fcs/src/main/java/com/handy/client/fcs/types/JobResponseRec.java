package com.handy.client.fcs.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class JobResponseRec {

    @JacksonXmlProperty(localName = "jobId")
    private String jobId;

    @JacksonXmlProperty(localName = "jobNo")
    private String jobNo;

    @JacksonXmlProperty(localName = "serviceDaTi")
    private Date serviceDaTi;

    @JacksonXmlProperty(localName = "deadlineDaTi")
    private Date deadlineDaTi;

    @JacksonXmlProperty(localName = "deviceNo")
    private String deviceNo;

    @JacksonXmlProperty(localName = "deviceType")
    private String deviceType;

}
