package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class RoomChange extends BaseEntity {

    @JacksonXmlProperty(localName = "OldEx")
    private String oldEx;

    @JacksonXmlProperty(localName = "OldRm")
    private String oldRm;

    @JacksonXmlProperty(localName = "OldPId")
    private String oldPId;

    @JacksonXmlProperty(localName = "GShareFlag")
    private String gShareFlag;

    @JacksonXmlProperty(localName = "GFolNo")
    private String gFolNo;

    @JacksonXmlProperty(localName = "GLang")
    private String gLang;

    @JacksonXmlProperty(localName = "GName")
    private String gName;

    @JacksonXmlProperty(localName = "GLName")
    private String gLName;

    @JacksonXmlProperty(localName = "GFName")
    private String gFName;

    @JacksonXmlProperty(localName = "GTitle")
    private String gTitle;

    @JacksonXmlProperty(localName = "GNation")
    private String gNation;

    @JacksonXmlProperty(localName = "GDOB")
    private String gDoB;

    @JacksonXmlProperty(localName = "GId")
    private String gId;

    @JacksonXmlProperty(localName = "OldGShareFlag")
    private String oldGShareFlag;

    @JacksonXmlProperty(localName = "DND")
    private String dnd;

    @JacksonXmlProperty(localName = "MWL")
    private String mwl;

    @JacksonXmlProperty(localName = "COSCode")
    private String cosCode;




    @JacksonXmlProperty(localName = "GGrpCode")
    private String gGrpCode;

    @JacksonXmlProperty(localName = "GVIP")
    private String gVip;

    @JacksonXmlProperty(localName = "CIDaTi")
    private Date ciDaTi;

    @JacksonXmlProperty(localName = "CODaTi")
    private Date coDaTi;

}
