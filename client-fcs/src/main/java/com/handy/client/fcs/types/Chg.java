package com.handy.client.fcs.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class Chg {

    @JacksonXmlProperty(localName = "CRate", isAttribute = true)
    private String cRate;

    @JacksonXmlProperty(localName = "Cost", isAttribute = true)
    private String cost;

    @JacksonXmlProperty(localName = "Basic", isAttribute = true)
    private String basic;

    @JacksonXmlProperty(localName = "Sur", isAttribute = true)
    private String sur;

    @JacksonXmlProperty(localName = "Tax", isAttribute = true)
    private String tax;

    @JacksonXmlProperty(localName = "Svr", isAttribute = true)
    private String svr;

    @JacksonXmlProperty(localName = "Ref", isAttribute = true)
    private String ref;

    @JacksonXmlProperty(localName = "Tot", isAttribute = true)
    private String tot;

    @JacksonXmlProperty(localName = "Tip", isAttribute = true)
    private String tip;

    @JacksonXmlProperty(localName = "Svc", isAttribute = true)
    private String svc;

    @JacksonXmlProperty(localName = "Disc", isAttribute = true)
    private String disc;

}


