package com.handy.client.fcs.services;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;

@Slf4j
@Service
public class FcsClient {

    private Socket socket;

    private BufferedInputStream in;
    private BufferedOutputStream out;
    private ReceiveHandler handler;

    public void connect(String host, int port) throws IOException {
        InetSocketAddress addr = new InetSocketAddress(host, port);

        socket = new Socket();
        socket.connect(addr);

        in = new BufferedInputStream(socket.getInputStream());
        out = new BufferedOutputStream(socket.getOutputStream());

        // setup listening thread
        handler = new ReceiveHandler(in)  {

            @Override
            public void onCommandReceived(byte[] buffer) {
                FcsClient.this.onCommandReceived(new String(buffer));
            }

        };

        new Thread(handler).start();
    }

    public void sendCommand(String command) throws IOException {
        out.write(Constant.STX);
        out.write(command.getBytes());
        out.write(Constant.ETX);

        out.flush();
    }

    public void onCommandReceived(String command) {
        log.info(command);
    }

    public void close() {
        handler.stop();

        closeQuietly(in);
        closeQuietly(out);
        closeQuietly(socket);
    }

    private void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                // ignore
            }
        }
    }

}