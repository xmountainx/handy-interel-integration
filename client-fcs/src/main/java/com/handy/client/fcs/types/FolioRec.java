package com.handy.client.fcs.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class FolioRec {

    @JacksonXmlProperty(localName = "billDeptNo")
    private String billDeptNo;

    @JacksonXmlProperty(localName = "billDaTi")
    private Date billDaTi;

    @JacksonXmlProperty(localName = "billDesc")
    private String billDesc;

    @JacksonXmlProperty(localName = "billItemPrice")
    private String billItemPrice;

}
