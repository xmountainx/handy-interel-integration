package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement(localName = "CallCharge")
public class CallCharge {

    @JacksonXmlProperty(localName = "Ex")
    private String ex;

    @JacksonXmlProperty(localName = "Rm")
    private String rm;

    @JacksonXmlProperty(localName = "PId")
    private String pid;

    @JacksonXmlProperty(localName = "PostType")
    private String postType;

    @JacksonXmlProperty(localName = "Tel")
    private String tel;

    @JacksonXmlProperty(localName = "Dur")
    private String dur;

    @JacksonXmlProperty(localName = "Trk")
    private String trk;

    @JacksonXmlProperty(localName = "Auth")
    private String auth;

    @JacksonXmlProperty(localName = "Carrier")
    private String carrier;

    @JacksonXmlProperty(localName = "TransDaTi")
    private Date transDaTi;

    @JacksonXmlProperty(localName = "SysDaTi")
    private Date sysDaTi;

    @JacksonXmlProperty(localName = "GUID")
    private String guid;

}
