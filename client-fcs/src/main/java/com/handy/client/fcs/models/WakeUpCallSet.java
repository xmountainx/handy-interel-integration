package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement(localName = "Wakeup")
public class WakeUpCallSet extends BaseEntity {

    @JacksonXmlProperty(localName = "WUSts")
    private String wuSts;

    @JacksonXmlProperty(localName = "WUDaTi")
    private Date wuDaTi;

    @JacksonXmlProperty(localName = "WURecur")
    private String wuRecur;

}
