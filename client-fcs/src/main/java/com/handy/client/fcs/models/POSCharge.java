package com.handy.client.fcs.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import com.handy.client.fcs.types.Chg;
import com.handy.client.fcs.types.POSRec;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class POSCharge extends BaseEntity {

    @JacksonXmlProperty(localName = "PostType")
    private String postType;

    @JacksonXmlProperty(localName = "PayType")
    private String payType;

    @JacksonXmlProperty(localName = "CheckIdent")
    private String checkIdent;

    @JacksonXmlProperty(localName = "RevenueId")
    private String revenueId;

    @JacksonXmlProperty(localName = "TransNo")
    private String transNo;

    @JacksonXmlProperty(localName = "CheckId")
    private String checkId;

    @JacksonXmlElementWrapper(localName = "POSRecSec")
    private List<POSRec> posRecSec;

    @JacksonXmlProperty(localName = "Chg")
    private Chg chg;

    @JacksonXmlProperty(localName = "TransDaTi")
    private String transDaTi;

}
