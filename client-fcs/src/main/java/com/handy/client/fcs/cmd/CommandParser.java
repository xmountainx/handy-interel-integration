package com.handy.client.fcs.cmd;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Component
public class CommandParser {

    @Autowired
    private XmlMapper mapper;

    public <T> T parse(String str, Class<T> clazz) throws IOException {
        return mapper.readValue(str, clazz);
    }

}
