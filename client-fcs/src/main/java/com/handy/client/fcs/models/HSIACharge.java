package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import com.handy.client.fcs.types.Chg;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class HSIACharge extends BaseEntity {

    @JacksonXmlProperty(localName = "PostType")
    private String postType;

    @JacksonXmlProperty(localName = "Dur")
    private String dur;

    @JacksonXmlProperty(localName = "Chg")
    private Chg chg;

    @JacksonXmlProperty(localName = "KByte")
    private String kByte;

    @JacksonXmlProperty(localName = "TransDaTi")
    private Date transDaTi;

}
