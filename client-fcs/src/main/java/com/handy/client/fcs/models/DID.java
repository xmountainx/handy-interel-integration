package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class DID extends BaseEntity {

    @JacksonXmlProperty(localName = "GFolNo")
    private String gFolNo;

    @JacksonXmlProperty(localName = "DIDType")
    private String didType;

    @JacksonXmlProperty(localName = "GDIDTel")
    private String gDidTel;

}
