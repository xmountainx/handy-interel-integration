package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ser.Serializers;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class DoNotDisturb extends BaseEntity {

    @JacksonXmlProperty(localName = "DND")
    private String dnd;

    @JacksonXmlProperty(localName = "Sched")
    private String sched;

    @JacksonXmlProperty(localName = "DNDDaTi")
    private Date dndDaTi;

}
