package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class SendAdHocMsg {


    @JacksonXmlProperty(localName = "PId")
    private String pid;

    @JacksonXmlProperty(localName = "JobAction")
    private String jobAction;

    @JacksonXmlProperty(localName = "UserName")
    private String userName;

    @JacksonXmlProperty(localName = "Password")
    private String password;

    @JacksonXmlProperty(localName = "LangCode")
    private String langCode;

    @JacksonXmlProperty(localName = "MesgType")
    private String mesgType;

    @JacksonXmlProperty(localName = "Rm")
    private String rm;

    @JacksonXmlProperty(localName = "LocationCode")
    private String locationCode;

    @JacksonXmlProperty(localName = "RunnerId")
    private String runnerId;

    @JacksonXmlProperty(localName = "GroupId")
    private String groupId;

    @JacksonXmlProperty(localName = "DeviceType")
    private String deviceType;

    @JacksonXmlProperty(localName = "DeviceNo")
    private String deviceNo;

    @JacksonXmlProperty(localName = "ScheduleDaTi")
    private Date scheduleDaTi;

    @JacksonXmlProperty(localName = "MesgContent")
    private String mesgContent;

    @JacksonXmlProperty(localName = "SysDaTi")
    private Date sysDaTi;

    @JacksonXmlProperty(localName = "GUID")
    private String guid;

}
