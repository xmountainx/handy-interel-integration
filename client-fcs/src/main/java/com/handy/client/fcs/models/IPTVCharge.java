package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class IPTVCharge extends BaseEntity {

    @JacksonXmlProperty(localName = "PostType")
    private String postType;

    @JacksonXmlProperty(localName = "Dur")
    private String dur;

    @JacksonXmlProperty(localName = "Desc")
    private String desc;

    @JacksonXmlProperty(localName = "Chg")
    private String chg;

    @JacksonXmlProperty(localName = "TransDaTi")
    private String transDaTi;

}
