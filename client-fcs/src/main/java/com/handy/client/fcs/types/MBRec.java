package com.handy.client.fcs.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class MBRec {

    @JacksonXmlProperty(localName = "Code", isAttribute = true)
    private String code;

    @JacksonXmlProperty(localName = "Price", isAttribute = true)
    private String price;

    @JacksonXmlProperty(localName = "Qty", isAttribute = true)
    private String qty;

    @JacksonXmlProperty(localName = "Desc", isAttribute = true)
    private String desc;

    @JacksonXmlProperty(localName = "SubTot", isAttribute = true)
    private String subTot;

    @JacksonXmlProperty(localName = "Tax", isAttribute = true)
    private String tax;

    @JacksonXmlProperty(localName = "Svr", isAttribute = true)
    private String svr;

}

