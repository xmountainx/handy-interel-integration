package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class DBSwap {

    @JacksonXmlProperty(localName = "PId")
    private String pid;

    @JacksonXmlProperty(localName = "Flag")
    private String flag;

    @JacksonXmlProperty(localName = "SysDaTi")
    private String sysDaTi;

    @JacksonXmlProperty(localName = "GUID")
    private String guid;

}
