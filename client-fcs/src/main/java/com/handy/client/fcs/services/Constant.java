package com.handy.client.fcs.services;

public interface Constant {

    int ETX = 0x03;
    int STX = 0x02;
    int ACK = 0x06;
    int NAK = 0x15;

    int BUFFER_SIZE = 0xffff;

}
