package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class GuestExpressCO extends BaseEntity {

    @JacksonXmlProperty(localName = "Flag")
    private String flag;

    @JacksonXmlProperty(localName = "Desc")
    private String desc;

    @JacksonXmlProperty(localName = "GFolNo")
    private String gFolNo;

    @JacksonXmlProperty(localName = "TotBillAmt")
    private String totBillAmt;

    @JacksonXmlProperty(localName = "AnsSts")
    private String ansSts;

}
