package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class MesgLamp extends BaseEntity {

    @JacksonXmlProperty(localName = "MWType")
    private String mwType;

    @JacksonXmlProperty(localName = "MWL")
    private String mwl;

    @JacksonXmlProperty(localName = "GFolNo")
    private String gFolNo;

    @JacksonXmlProperty(localName = "TMCount")
    private String tmCount;

    @JacksonXmlProperty(localName = "VMCount")
    private String vmCount;

    @JacksonXmlProperty(localName = "FMCount")
    private String fmCount;


}
