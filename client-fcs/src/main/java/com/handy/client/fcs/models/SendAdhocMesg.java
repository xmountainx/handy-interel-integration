package com.handy.client.fcs.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.handy.client.fcs.types.BaseEntity;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JacksonXmlRootElement
public class SendAdhocMesg extends BaseEntity {

    @JacksonXmlProperty(localName = "AnsSts")
    private String ansSts;

    @JacksonXmlProperty(localName = "Flag")
    private String flag;

    @JacksonXmlProperty(localName = "MesgId")
    private String mesgId;

    @JacksonXmlProperty(localName = "ErrorMsg")
    private String errorMsg;

}
