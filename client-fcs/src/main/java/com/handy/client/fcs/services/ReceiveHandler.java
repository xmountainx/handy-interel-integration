package com.handy.client.fcs.services;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
public abstract class ReceiveHandler implements Runnable {

    private Lock lock = new ReentrantLock();

    private boolean listening;
    private boolean waiting;
    private InputStream in = null;

    public ReceiveHandler(InputStream in) {
        this.in = in;
    }

    @Override
    public void run() {
        int startByte;
        int nextByte;
        int count;

        listening = true;
        waiting = false;

        while (listening) {
            try {
                startByte = in.read();

                if (startByte != Constant.STX) {
                    continue;
                }

                count = 0;

                byte[] buffer = new byte[Constant.BUFFER_SIZE];

                while ((nextByte = in.read()) != Constant.ETX) {
                    if (count >= buffer.length) {
                        log.info("message length excess buffer, drop packet.");
                        break;
                    }

                    buffer[count++] = (byte) nextByte;
                }

                onCommandReceived(Arrays.copyOfRange(buffer, 0, count));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void stop() {
        listening = false;
    }

    public void waitResponse() throws InterruptedException {
        waiting = true;

//        lock.newCondition()
    }

    public abstract void onCommandReceived(byte[] buffer);

}
