package com.handy.client.fcs.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class Org {

    @JacksonXmlProperty(localName = "dept", isAttribute = true)
    private String dept;

    @JacksonXmlProperty(localName = "div", isAttribute = true)
    private String div;

    @JacksonXmlProperty(localName = "sect", isAttribute = true)
    private String sect;

}
