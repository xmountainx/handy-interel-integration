package com.handy.client.fcs.config;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.text.SimpleDateFormat;


@Configuration
public class AppConfig {

    @Bean
    public XmlMapper xmlMapper() {
        JacksonXmlModule module = new JacksonXmlModule();
        XmlMapper xmlMapper = new XmlMapper(module);

        xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
        xmlMapper.setDateFormat(new SimpleDateFormat("YYYY/MM/dd HH:mm:ss"));

        return xmlMapper;
    }

}
