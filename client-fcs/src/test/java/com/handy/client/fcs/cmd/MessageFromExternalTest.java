package com.handy.client.fcs.cmd;

import com.handy.client.fcs.models.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageFromExternalTest {

    @Autowired
    private CommandParser parser;

    @Test
    public void parseRoomStatus() throws Exception {

        Object obj = parser.parse(
                "<RoomStatus><PId>MO5</PId><Rm>13061</Rm><Ex>713061</Ex><RSCode>VC</RSCode><TransDaTi> 2012/09/20 00:00:51</TransDaTi><SysDaTi>2012/09/20 00:00:51</SysDaTi> <OldRSCode>VD</OldRSCode><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></RoomStatus>",
                RoomStatus.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseMTCharge() throws Exception {

        Object obj = parser.parse(
                "<MTCharge> <Ex>4567</Ex><PId>1</PId><SysDaTi>2012/8/1 12:11:00</SysDaTi><TransDaTi>2010/8/1 12:10:00</TransDaTi><Chg Tot=\"12.00\" /><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></MTCharge>",
                MTCharge.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseMICharge() throws Exception {

        Object obj = parser.parse(
                "<MICharge><Ex>8567</Ex><Rm>567</Rm><PId>1</PId> <SysDaTi>2008/8/1 08:11:07</SysDaTi> <TransDaTi>2008/8/1 08:09:00</TransDaTi><MaidId>232</MaidId><MaidPIN>1234</MaidPIN><MBRecSec><MBRec Code=\"03\" Price=\"2.5\" Desc=\"Coke\" Qty=\"2\" SubTot=\"5\"/><MBRec Code=\"14\" Price=\"7.00\" Desc=\"Chips\" Qty=\"1\" SubTot=\"7.00\"/> </MBRecSec><Chg Tot=\"12.00\" /><GUID>E1A35954-41CC-40f7- BDF1-A7B2CE1EE5B6</GUID></MICharge>",
                MICharge.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseCallCharge() throws Exception {

        Object obj = parser.parse(
                "<CallCharge><PId>02</PId><PostType>CC</PostType><Ex>50507</Ex><Tel>28783380</Tel><Dur>60</Dur><Trk>023</Trk><Auth>2468</Auth><TransDaTi>2012/05/25 07:11:00</TransDaTi><SysDaTi>2012/05/25 16:29:10</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></CallCharge>",
                CallCharge.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parsePOSCharge() throws Exception {

        Object obj = parser.parse(
                "<POSCharge><Rm>1234</Rm><SysDaTi>2010/11/30 12:11:09</SysDaTi><TransDaTi>2010/11/30 12:11:07</TransDaTi><PId>1</PId><PostType>PO</PostType> <PayType>CR</PayType><CheckIdent>9876</CheckIdent><RevenueId>11</RevenueId> <TransNo>0000001478</TransNo><CheckId>13:45:54. 149</CheckId><POSRecSec><POSRec EntryType=\"M\" Code=\"03\" Price=\"12.00\" Desc=\"Orange Juice\" Disc=\"0\" Qty=\"2\" SubTot=\"24.00\" /><POSRec EntryType=\"C\" Code=\"222\" Price=\"0\" Desc=\"tomato source\" Disc=\"0\" Qty=\"1\" SubTot=\"0\" /><POSRec EntryType=\"M\" Code=\"14\" Price=\"88.90\" Desc=\"Buffet Lunch\" Disc=\"0.45\" Qty=\"1\" SubTot=\"88.90\"/><POSRec EntryType=\"M\" Code=\"7\" Price=\"12.50\" Desc=\"Banana split\" Disc=\"\" Qty=\"1\" SubTot=\"12.50\" Ref=\"Nut allergy\"/></POSRecSec><Chg Tip=\"0\" Tax=\"5.2\" Svc=\"10.39\" Disc=\"0.45\" Tot=\"119.49\"/><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></POSCharge>",
                POSCharge.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseHSIACharge() throws Exception {

        Object obj = parser.parse(
                "<HSIACharge ><PId>02</PId><PostType>PI</PostType><Rm>507</Rm><Dur>6754</Dur> <Chg Tot=\"10.00\" /> <TransDaTi>2012/05/25 07:11:00</TransDaTi><SysDaTi>2012/05/25 16:29:10</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></HSIACharge>",
                HSIACharge.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseIPTVCharge() throws Exception {

        Object obj = parser.parse(
                "<IPTVCharge ><PId>02</PId><PostType>VC</PostType><Rm>507</Rm> <Dur>9200</Dur><Chg Tot=\"10.00\" /> <Desc>Spider Man 3</Desc><TransDaTi>2012/05/25 07:11:00</TransDaTi><SysDaTi>2012/05/25 16:29:10</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1- A7B2CE1EE5B6</GUID></IPTVCharge>",
                IPTVCharge.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseVoiceMessage() throws Exception {

        Object obj = parser.parse(
                "<VoiceMessage> <Ex>8100</Ex><PId>1</PId><VMCount>0</VMCount><SysDaTi>2012/8/4 10:00:00</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></VoiceMessage>",
                VoiceMessage.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseWakeUpCallSet() throws Exception {

        Object obj = parser.parse(
                "<Wakeup><Ex>8100</Ex><PId>1</PId><WUDaTi>2012/8/5 07:00:00</WUDaTi><WURecur>4</WURecur><WUSts>WR</WUSts><SysDaTi>2012/8/4 08:11:07</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></Wakeup>",
                WakeUpCallSet.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseWakeUpCallResponse() throws Exception {

        Object obj = parser.parse(
                "<Wakeup><Ex>8100</Ex><PId>1</PId><WUDaTi>2012/8/5 07:00:00</WUDaTi><WUSts>WA</WUSts><SysDaTi>2012/8/5 07:00:17</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1- A7B2CE1EE5B6</GUID></Wakeup>",
                WakeUpCallResponse.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseDBSwap() throws Exception {

        Object obj = parser.parse(
                "<DBSwap><PId>1</PId><Flag>RQ</Flag><SysDaTi>2012/12/15 07:00:17</SysDaTi><GUID>E1A35954- 41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></DBSwap>",
                DBSwap.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseGuestExpressCO() throws Exception {

        Object obj = parser.parse(
                "<GuestExpressCO><Rm>100</Rm><Ex>8100</Ex><PId>1</PId><Flag>RQ</Flag><GFolNo>123456</GFolNo><TotBillAmt>5569.09</TotBillAmt><SysDaTi>2012/8/4 08:11:07</SysDaTi><GUID>E1A35954-41CC- 40f7-BDF1-A7B2CE1EE5B6</GUID></GuestExpressCO>",
                GuestExpressCO.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseGuestFolioDetail() throws Exception {

        Object obj = parser.parse(
                "<GuestFolioDetail><Rm>100</Rm><Ex>8100</Ex><PId>1</PId><Flag>RQ</Flag><GFolNo>123456</GFolNo><SysDaTi>2012/8/4 08:11:07</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1- A7B2CE1EE5B6</GUID></GuestFolioDetail>",
                GuestFolioDetail.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseGuestMessage() throws Exception {

        Object obj = parser.parse(
                "<GuestMessage ><Rm>100</Rm><PId>1</PId><MesgType>RQ</MesgType><SysDaTi>2012/8/4 08:11:07</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></GuestMessage >",
                GuestMessage.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseCreateJob() throws Exception {

        Object obj = parser.parse(
                "<CreateJob><JobAction>Immediate</JobAction><UserName>fcs</UserName><Password>pwd</Password><PId>1</PId><Rm>1001</Rm><ItemCode>10100</ItemCode><Rem>This is urgent </Rem> <CreatedByUserName>Jason Chan</CreatedByUserName><ReportedByName>Mr. Lim</ReportedByName><RunnerUserName>81088</RunnerUserName><SysDaTi>2012/8/4 08:11:07</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></CreateJob>",
                CreateJob.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseUpdateJob() throws Exception {

        Object obj = parser.parse(
                "<UpdateJob><JobAction>Ack</JobAction><UserName>fcs</UserName><Password>pwd</Password><PId>1</PId><ItemCode>10100</ItemCode><JobId>123</JobId><SysDaTi>2012/8/4 08:11:07</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></UpdateJob>",
                UpdateJob.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseSendAdHocMsg() throws Exception {

        Object obj = parser.parse(
                "<SendAdHocMsg><UserName>fcs</UserName><Password>pwd</Password><PId>1</PId><MesgContent>This is urgent </MesgContent><DeviceType>MBL</DeviceType><DeviceNo>0122101234</DeviceNo> <MesgType>NM</MesgType><SysDaTi>2012/8/4 08:11:07</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></SendAdHocMsg>",
                SendAdHocMsg.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseStayAlive() throws Exception {

        Object obj = parser.parse(
                "<StayAlive><SysDaTi>2012/8/4 08:11:07</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1- A7B2CE1EE5B6</GUID></StayAlive>",
                StayAlive.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

}