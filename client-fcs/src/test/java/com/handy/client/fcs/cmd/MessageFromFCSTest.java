package com.handy.client.fcs.cmd;

import com.handy.client.fcs.models.*;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageFromFCSTest {

    @Autowired
    private CommandParser parser;


    @Test
    public void parseCheckIn() throws Exception {

        Object obj = parser.parse(
                "<CheckIn><PId>MO1</PId><Rm>11941</Rm><Ex>711941</Ex><CIDaTi>2012/09/19 00:16:47</CIDaTi><CODaTi>2012/09/21 12:00:00</CODaTi><GFolNo>37518</GFolNo><GShareFlag>No</GShareFlag><GGrpCode/><GLang>EN</GLang><GTitle>Ms.</GTitle><GName>Ms. Meri</GName><GFName>Riin</GFName><GLName>Meri</GLName><SysDaTi>2012/09/20 00:16:47</SysDaTi><VidRight>VA</VidRight><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></CheckIn>",
                CheckIn.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseCheckOut() throws Exception {

        Object obj = parser.parse(
                "<CheckOut><PId>MO5</PId><Rm>11906</Rm><Ex>711906</Ex><GShareFlag>No</GShareFlag><SwapFlag>Yes</SwapFlag><SysDaTi>2012/09/20 03:28:24</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1- A7B2CE1EE5B6</GUID></CheckOut>",
                CheckOut.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseMesgLamp() throws Exception {

        Object obj = parser.parse(
                "<MesgLamp><PId>MO5</PId><Rm>12324</Rm><Ex>712324</Ex><MWL>Off</MWL><MWType>T</MWType><TMCount>0</TMCount><SysDaTi>2012/09/20 07:29:17</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></MesgLamp>",
                MesgLamp.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseDoNotDisturb() throws Exception {

        Object obj = parser.parse(
                "<DoNotDisturb><PId>MO5</PId><Rm>13335</Rm><Ex>713335</Ex><DND>Off</DND><DNDDaTi>2012/09/20 07:50:40</DNDDaTi>><SysDaTi>2012/09/20 07:50:40</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></DoNotDisturb>",
                DoNotDisturb.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseAutoWakeUpCallSet() throws Exception {

        Object obj = parser.parse(
                "<Wakeup><Ex>8100</Ex><PId>1</PId><WUDaTi>2012/8/5 07:00:00</WUDaTi><WURecur>4</WURecur><WUSts>WR</WUSts><SysDaTi>2012/8/4 08:11:07</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></Wakeup>",
                WakeUpCallSet.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseRoomChange() throws Exception {

        Object obj = parser.parse(
                "<RoomChange><PId>MO5</PId><OldPId>MO5</OldPId><Rm>12644</Rm><Ex>712644</Ex><OldRm>12744</OldRm><OldEx>712744</OldEx><GFolNo>58691</GFolNo><GShareFlag>No</GShareFlag><GGrpCode>57861</GGrpCode><GLang>MA</GLang><GTitle>Mr.</GTitle><GName> Chen</GName><GFName>Shu Yi</GFName><GLName>Chen</GLName><GDOB>1980/6/26</GDOB><GNation>CN</GNation><SysDaTi>2012/09/20 12:47:56</SysDaTi><GVIP></GVIP><CIDaTi>9/20/2012 12:45:28 PM</CIDaTi><CODaTi>9/23/2012 12:00:00 PM</CODaTi><MWL>No</MWL><DND>No</DND><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></RoomChange>",
                RoomChange.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseInfoChange() throws Exception {

        Object obj = parser.parse(
                "<InfoChange><PId>MO5</PId><Rm>13302</Rm><Ex>713302</Ex><CIDaTi>2012/09/19 07:22:55</CIDaTi><CODaTi>2012/09/22 12:00:00</CODaTi><GFolNo>53294</GFolNo><GShareFlag>No</GShareFlag><GLang>EN</GLang><GVIP>2</GVIP><GTitle>Mr.</GTitle><GName>Mr. Dolp</GName><GFName>Josef</GFName><GLName>Dolp</GLName><GNation>AT</GNation><SysDaTi>2012/09/20 07:22:55</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1- A7B2CE1EE5B6</GUID></InfoChange>",
                InfoChange.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseDID() throws Exception {

        Object obj = parser.parse(
                "<DID><PId>MO5</PId><Rm>12612</Rm><Ex>712612</Ex><DIDType>AT</DIDType><GDIDTel>1234567</GDIDTel><SysDaTi>2012/09/20 12:48:28</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></DID>",
                DID.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseClassOfService() throws Exception {

        Object obj = parser.parse(
                "<ClassOfService><PId>MO5</PId><Rm>12612</Rm><Ex>712612</Ex><COSCode>00</COSCode><SysDaTi>2012/09/20 12:48:28</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></ClassOfService>",
                ClassOfService.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseGuestExpressCO() throws Exception {

        Object obj = parser.parse(
                "<GuestExpressCO><Rm>100</Rm><Ex>8100</Ex><PId>1</PId><Flag>RP</Flag><AnsSts>OK</AnsSts><GFolNo>123456</GFolNo><TotBillAmt>5569.09</TotBillAmt><SysDaTi>2012/8/4 08:11:07</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></GuestExpressCO>",
                GuestExpressCO.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }


    @Test
    public void parseGuestFolioDetail() throws Exception {

        Object obj = parser.parse(
                "<GuestFolioDetail><Rm>100</Rm><Ex>8100</Ex><PId>1</PId><Flag>RP</Flag><AnsSts>OK</AnsSts><GFolNo>123456</GFolNo><TotBillAmt>5569.09</TotBillAmt><SysDaTi>2012/8/4 08:11:07</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></GuestFolioDetail>",
                GuestFolioDetail.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseGuestMessage() throws Exception {

        Object obj = parser.parse(
                "<GuestMessage><Rm>100</Rm><PId>1</PId><GFolNo>123456</GFolNo><MesgId>19</MesgId><MesgType>RY</MesgType><MesgContent>Please call back office asap</MesgContent><MesgDaTi>2008/8/4 08:11:07</MesgDaTi><SysDaTi>2012/8/4 08:11:07</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></GuestMessage>",
                GuestMessage.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseDBSwap() throws Exception {

        Object obj = parser.parse(
                "<DBSwap><PId>1</PId><Flag>ST</Flag><SysDaTi>2012/12/15 07:00:17</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></DBSwap>",
                DBSwap.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseNightAudit() throws Exception {

        Object obj = parser.parse(
                "<NightAudit><PId>1</PId><Flag>ST</Flag><SysDaTi>2012/12/15 07:00:17</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></NightAudit>\n",
                NightAudit.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Ignore
    @Test
    public void parseCreateJobResponse() throws Exception {

        Object obj = parser.parse(
                "",
                CreateJobResponse.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Ignore
    @Test
    public void parseUpdateJobResponse() throws Exception {

        Object obj = parser.parse(
                "",
                UpdateJobResponse.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseModifyJobResponse() throws Exception {

        Object obj = parser.parse(
                "<ModifyJob><Rm>100</Rm><Ex>8100</Ex><PId>1</PId><JobNo>21001</JobNo><JobDetail>Check in welcome drinks</JobDetail><EscLevel>0</EscLevel><ItemCode>100011</ItemCode><ItemName>Welcome Drink</ItemName><AssignedTo>ABC</AssignedTo><Deadline>2012/8/4 08:30:00</Deadline><GuestName>Mr.Dolp</GuestName><LocationName>Room</LocationName><JobStatus>Pending</JobStatus><ACKStatus>1</ACKStatus><Sender>GSS2</Sender><SysDaTi>2012/8/4 08:11:07</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1-A7B2CE1EE5B6</GUID></ModifyJob>",
                ModifyJob.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Ignore
    @Test
    public void parseSendAdhocMesg() throws Exception {

        Object obj = parser.parse(
                "",
                SendAdhocMesg.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }

    @Test
    public void parseStayAlive() throws Exception {

        Object obj = parser.parse(
                "<StayAlive><SysDaTi>2012/8/4 08:11:07</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1- A7B2CE1EE5B6</GUID></StayAlive>",
                StayAlive.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);

    }


    @Test
    public void parseErrorMessage() throws Exception {

        Object obj = parser.parse(
                "<ErrorMessage><AnsSts>MI</AnsSts><Desc>Missing ‘Chg’ element in ‘IPTVCharge’ packet</Desc><SysDaTi>2012/8/4 08:11:07</SysDaTi><GUID>E1A35954-41CC-40f7-BDF1- A7B2CE1EE5B6</GUID></ErrorMessage>",
                ErrorMessage.class
        );

        System.out.println(obj);
        Assert.assertNotNull(obj);
    }

}
